// Definició del component

var tempsComponent = {
    template:
        `
      <div class="container">

      <div class="row justify-content-between ">

        <!--IZQUIERDA-->
        <div class="col-4">
          <div class="row ">
            <div class="col-12 bg-white-50 py-3 text-white text-center">
              <h3 class="mb-0 text-uppercase">{{continent}}</h3>
            </div>
          </div>
        </div>

        <!--DERECHA-->
        <div class="col-7 bg-white-50 py-3 text-white "> <!--esto es como si fuera un contenedor-->
          <select class="form-control" aria-label="" v-model="selectedCity">
            <option class="" v-for="ciutat in llistaCiutats">{{ciutat}}</option>
          </select>
        </div>

      </div>

      <div class="row justify-content-between py-3">
        <!--IZQUIERDA-->
        <div class="col-4"></div>
        <!--DERECHA-->
        <div class="col-7"> <!--esto es como si fuera un contenedor-->
          <div class="row justify-content-end">
            <div class="col-6 py-3 text-white">
              <div class="col-12 bg-white-50 altezza">
                <div class="row">
                  <div class="col-12 pt-3 text-center ">
                    <h5>{{ciutatActual.name}}</h5>
                  </div>
                </div>
                <div class="row justify-content-md-center ">
                  <div class="col-12 col-sm-3 pr-0 mb-4 ">
                    <img v-bind:src="'http://openweathermap.org/img/wn/' + icon + '@2x.png' "  />
                  </div>
                  <div class="col-12 col-sm-9  mt-3 text-center pl-0 ">
                    <h1><strong>{{ciutatActual['main']['temp']}}°C </strong></h1>
                    <span>- {{ciutatActual.weather[0].description }}</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-6  py-3 ">
              <div class="col-12 bg-white-50 altezza">
                <div class="row align-items-center text-white " >
                  <div class="col-6 mt-60">
                    <span>minima: <strong>{{ciutatActual['main']['temp_min']}}°C </strong></span>
                    <span>maxima: <strong>{{ciutatActual['main']['temp_max']}}°C</strong> </span>
                  </div>
                  <div class="col-6 mt-60">
                    <span>pression: <strong>{{ciutatActual['main']['pressure']}}</strong></span>
                    <span>humidita: <strong>{{ciutatActual['main']['humidity']}}</strong></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      </div>

     

    `,

    props:['llistaCiutats', 'continent' ],

    data: function () {
        return {
            ciutatActual: null,
            selectedCity: null,
            icon: null,
        }
    },

    methods: {
        async getWeather(ciutat) {
            var url = 'https://api.openweathermap.org/data/2.5/weather?q=' + ciutat + '&units=metric&lang=ca&appid=644da4f2a1231c6611d2e2d8abb1fc90';

            try {
                var response = await fetch(url);
                var data = await response.json();
                this.ciutatActual = data;
                this.icon = data['weather'][0]['icon'];
            }
            catch(err) {
                console.log(err);
            }
        }
    },

    mounted: function () {
        this.selectedCity = this.llistaCiutats[0];
    },

    watch: {
        selectedCity () {
            this.getWeather(this.selectedCity);
        }
    }
};


var vm = new Vue({
    el: '#eltemps',
    data: {
        selectedContinente: "todos",
        toggled: false,
        fechaDia: null,
        fechaMes: null,
        fechaYear: null,
        continentes:[
            "todos",
            "europa",
            "asia",
            "america",
            "africa",
        ],
        europa: [
            "Barcelona",
            "Lleida",
            "Zaragoza",
            "Sevilla",
            "Madrid",
            "Paris",
        ],
        asia:[
            "Lahore",
            "Beijing",
            "Kabul",
            "Tehran"
        ],
        america:[
            "Mountain View",
            "New Jersey",
            "Panama",
        ],
        africa:[
            "Lagos",
            "Cairo",
            "Luanda",
        ],
        monthNames: [
            "enero",
            "febrero",
            "marzo",
            "abril",
            "mayo",
            "junio",
            "julio",
            "agosto",
            "septiembre",
            "octubre",
            "noviembre",
            "deciembre"],
    },


    created: function () {
        const d = new Date()
        this.fechaDia=d.getDate();
        this.fechaMes=this.monthNames[d.getMonth()];
        this.fechaYear=d.getFullYear();
    },
    methods: {
        hamburgerMenu(){
            this.toggled = !this.toggled;
        },
        closeHamburgerMenu(){
            $('.collapse').collapse('hide');
            this.hamburgerMenu()
        },
        selectContinent(continente) {
            this.selectedContinente = continente
        },
        showContinente(){
           return this.selectedContinente
        }
    },
    components:{
        'temps-component': tempsComponent
    }
    })





